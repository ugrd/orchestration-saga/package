package crypt

import "golang.org/x/crypto/bcrypt"

// HashMake is function to crypt
func HashMake(password string) string {
	bytes, _ := bcrypt.GenerateFromPassword([]byte(password), 14)

	return string(bytes)
}

// HashVerify is function to verify hashed
func HashVerify(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))

	return err == nil
}
