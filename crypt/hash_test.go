package crypt_test

import (
	"gitlab.com/ugrd/orchestration-saga/package/crypt"
	"reflect"
	"testing"
)

func TestHashMake(t *testing.T) {
	r := crypt.HashMake("hello")

	if reflect.ValueOf(r).IsZero() {
		t.Errorf("expected encrypted string, got empty string %s", r)
	}
}

func TestHashVerify(t *testing.T) {
	txt := "H3ll0W0rld"

	encrypted := crypt.HashMake(txt)

	if !crypt.HashVerify(txt, encrypted) {
		t.Error("expected true, got false")
	}
}
