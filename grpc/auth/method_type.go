package auth

// MethodOwner is a type definition
type MethodOwner struct {
	Owners  []string `json:"owners"`
	Protect bool     `json:"protect"`
}

// MethodTypes is a type
type MethodTypes map[string]*MethodOwner
