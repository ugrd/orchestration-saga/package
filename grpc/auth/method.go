package auth

// MethodRPC is a struct
type MethodRPC struct {
	method      string
	owner       string
	methodOwner *MethodOwner
	listOwners  MethodTypes
}

// NewMethodRPC is a constructor
func NewMethodRPC(methods MethodTypes) *MethodRPC {
	return &MethodRPC{
		listOwners: methods,
	}
}

// SetMethod is a method
func (rm *MethodRPC) SetMethod(v string) *MethodRPC {
	rm.method = v

	return rm
}

// SetOwner is a method
func (rm *MethodRPC) SetOwner(v string) *MethodRPC {
	rm.owner = v

	return rm
}

// ValidMethod is a method
func (rm *MethodRPC) ValidMethod() bool {
	if rm.method == "" {
		return true
	}

	for k, r := range rm.listOwners {
		if rm.method == k {
			rm.methodOwner = r
			return true
		}
	}

	return false
}

// IsProtected is a method
func (rm *MethodRPC) IsProtected() bool {
	if rm.methodOwner == nil {
		return false
	}

	return rm.methodOwner.Protect
}

// Valid is a method
func (rm *MethodRPC) Valid() bool {
	if rm.owner == "" {
		return true
	}

	for _, r := range rm.methodOwner.Owners {
		if r == rm.owner {
			return true
		}
	}

	return false
}
