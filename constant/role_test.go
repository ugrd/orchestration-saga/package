package constant_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/ugrd/orchestration-saga/package/constant"
	"testing"
)

func TestROLE_String(t *testing.T) {
	usr := constant.User

	assert.Equal(t, "USER", usr.String())

	adm := constant.Admin

	assert.Equal(t, "ADMIN", adm.String())

	spadm := constant.SuperAdmin

	assert.Equal(t, "SUPER_ADMIN", spadm.String())
}
