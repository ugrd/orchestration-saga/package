package constant

// ACCESS is a type
type ACCESS uint8

const (
	User ACCESS = iota
	Admin
	SuperAdmin
)

// String is a method
func (ac ACCESS) String() string {
	return []string{
		"USER",
		"ADMIN",
		"SUPER_ADMIN",
	}[ac]
}
