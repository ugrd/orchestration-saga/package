package configurator

// Configurator is a type
type Configurator struct {
	Account *Service
	Booking *Service
	Payment *Service
	Catalog *Service
	Bridge  *Service
	Seats   *Service
}

// New is a constructor
func New() *Configurator {
	return &Configurator{
		Account: &Service{
			Name:    "Account",
			RPCPort: 8181,
		},
		Booking: &Service{
			Name:    "Booking",
			RPCPort: 8182,
		},
		Payment: &Service{
			Name:    "Payment",
			RPCPort: 8183,
		},
		Catalog: &Service{
			Name:    "Catalog",
			RPCPort: 8184,
		},
		Seats: &Service{
			Name:    "Seats",
			RPCPort: 8185,
		},
		Bridge: &Service{
			Name:     "Bridge",
			HTTPPort: 8080,
		},
	}
}
