package configurator

// Service is a struct
type Service struct {
	Name     string
	RPCPort  int
	HTTPPort int
}
