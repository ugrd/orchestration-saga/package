package exception

import (
	"gitlab.com/ugrd/orchestration-saga/package/utils"
	"google.golang.org/genproto/googleapis/rpc/errdetails"
	"google.golang.org/grpc/status"
	"strings"
)

// ErrMessageFromGrpc is a struct
type ErrMessageFromGrpc struct {
	Message     string `json:"message"`
	HTTPCode    int    `json:"http_code"`
	ErrorFields ErrorFields
}

// FromGRPCError is a function
func FromGRPCError(err error) *ErrMessageFromGrpc {
	errOut := &ErrMessageFromGrpc{}

	er := status.Convert(err)

	errOut.Message = er.Message()
	errOut.HTTPCode = utils.HTTPStatusFromCode(er.Code())

	for _, dt := range er.Details() {
		switch t := dt.(type) {
		case *errdetails.BadRequest:
			for _, violation := range t.GetFieldViolations() {
				errOut.ErrorFields = append(errOut.ErrorFields, ErrorField{
					Field:    violation.GetField(),
					Messages: strings.Split(violation.GetDescription(), ", "),
				})
			}
		}
	}

	return errOut
}
