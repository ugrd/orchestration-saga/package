package exception_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"testing"
)

func TestNewGRPCError(t *testing.T) {
	e := exception.NewGRPCError()

	assert.NotNil(t, e)
	assert.Nil(t, e.ErrorFields)
}
