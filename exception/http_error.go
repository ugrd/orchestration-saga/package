package exception

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/ugrd/orchestration-saga/package/utils"
	"net/http"
)

type Errors map[string]string

// HTTPError is a struct
type HTTPError struct {
	gin     *gin.Context
	code    int
	message string
	errors  ErrorFields
}

// HTTPErrorOutput is a struct
type HTTPErrorOutput struct {
	Service string      `json:"service"`
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Errors  ErrorFields `json:"errors"`
}

// NewHTTPError is a constructor
func NewHTTPError(c *gin.Context, err string) *HTTPError {
	return &HTTPError{
		gin:     c,
		message: err,
	}
}

// WithHTTPStatus is a method
func (er *HTTPError) WithHTTPStatus(httpStatus int) *HTTPError {
	er.code = httpStatus

	return er
}

// FromGRPCError is a method
func (er *HTTPError) FromGRPCError(err error) *HTTPError {
	r := FromGRPCError(err)

	er.code = r.HTTPCode
	er.message = r.Message
	er.errors = r.ErrorFields

	return er
}

// WithErrFields is a method
func (er *HTTPError) WithErrFields(errs map[string]interface{}) *HTTPError {
	for k, v := range errs {
		er.errors = append(er.errors, ErrorField{
			Field:    k,
			Messages: []string{v.(string)},
		})
	}

	return er
}

// JSON is a method
func (er *HTTPError) JSON() {
	if er.code == 0 {
		er.code = http.StatusInternalServerError
	}

	resp := HTTPErrorOutput{
		Service: utils.GetEnv("APP_NAME", ""),
		Code:    er.code,
		Message: er.message,
		Errors:  er.errors,
	}

	er.gin.Header("Accept-Language", "en")
	er.gin.JSON(er.code, resp)
}
