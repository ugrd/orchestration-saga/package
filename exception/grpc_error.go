package exception

import (
	"google.golang.org/genproto/googleapis/rpc/errdetails"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"os"
	"strings"
)

// ErrorGRPC is a struct
type ErrorGRPC struct {
	Service     string
	ErrorCode   *codes.Code
	Status      *status.Status
	Message     string
	ErrorFields ErrorFields
}

// NewGRPCError is a constructor
func NewGRPCError() *ErrorGRPC {
	return &ErrorGRPC{
		Service:     os.Getenv("APP_NAME"),
		ErrorFields: nil,
	}
}

// WithMsg is a method
func (eg *ErrorGRPC) WithMsg(msg string) *ErrorGRPC {
	eg.Message = msg

	return eg
}

// WithErrCode is a method
func (eg *ErrorGRPC) WithErrCode(c codes.Code) *ErrorGRPC {
	eg.ErrorCode = &c

	return eg
}

// WithErrFields is a method
func (eg *ErrorGRPC) WithErrFields(errs ErrorFields) *ErrorGRPC {
	eg.ErrorFields = errs

	return eg
}

// WithFieldsFromMap is a method
func (eg *ErrorGRPC) WithFieldsFromMap(errs map[string]interface{}) *ErrorGRPC {
	for k, v := range errs {
		eg.ErrorFields = append(eg.ErrorFields, ErrorField{
			Field:    k,
			Messages: []string{v.(string)},
		})
	}

	return eg
}

// Build is a method
func (eg *ErrorGRPC) Build() *ErrorGRPC {
	c := codes.InvalidArgument
	if eg.ErrorCode == nil {
		eg.ErrorCode = &c
	}

	m := eg.ErrorCode.String()
	if eg.Message == "" {
		eg.Message = m
	}

	eg.Status = status.New(*eg.ErrorCode, eg.Message)

	return eg
}

// Err is a method
func (eg *ErrorGRPC) Err() error {
	// default err code

	eg.Build()

	return eg.Status.Err()
}

// ErrList is a method to handle error list
func (eg *ErrorGRPC) ErrList() error {
	if eg.ErrorFields == nil {
		return eg.Status.Err()
	}

	br := &errdetails.BadRequest{}

	for _, el := range eg.ErrorFields {
		br.FieldViolations = append(br.FieldViolations, &errdetails.BadRequest_FieldViolation{
			Field:       el.Field,
			Description: strings.Join(el.Messages, ", "),
		})
	}

	eg.Build()

	st, _ := eg.Status.WithDetails(br)

	return st.Err()
}
