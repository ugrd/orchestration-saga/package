package definition

// FilterQuery is a struct for filtering query definition
type FilterQuery struct {
	Query string `json:"query"`
	Page  uint32 `json:"page"`
	Limit uint32 `json:"limit"`
}
