package logger

import (
	"github.com/sirupsen/logrus"
	"os"
)

// New is constructor
func New() *logrus.Logger {
	log := logrus.New()

	// Set the logger to print logs to the console
	log.SetFormatter(&logrus.TextFormatter{})
	log.SetOutput(os.Stdout)

	return log
}
