package client

import (
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/account"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/booking"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/catalog"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/payment"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/seats"
	"google.golang.org/grpc"
)

// Option is an option type
type Option func(srv *GrpcServiceClient)

// WithAccountClientService is an option function
func WithAccountClientService(conn *grpc.ClientConn) Option {
	return func(rpc *GrpcServiceClient) {
		rpc.Account.Access = account.NewAccessServiceClient(conn)
		rpc.Account.User = account.NewUserServiceClient(conn)
		rpc.Account.Agent = account.NewAgentServiceClient(conn)
	}
}

// WithCatalogClientService is an option function
func WithCatalogClientService(conn *grpc.ClientConn) Option {
	return func(rpc *GrpcServiceClient) {
		rpc.Catalog.Transport = catalog.NewTransportServiceClient(conn)
		rpc.Catalog.Class = catalog.NewClassServiceClient(conn)
		rpc.Catalog.Provider = catalog.NewProviderServiceClient(conn)
		rpc.Catalog.Passenger = catalog.NewPassengerServiceClient(conn)
		rpc.Catalog.TransportShelter = catalog.NewTransportShelterServiceClient(conn)
		rpc.Catalog.Ticket = catalog.NewTicketServiceClient(conn)
	}
}

// WithPaymentClientService is a function
func WithPaymentClientService(conn *grpc.ClientConn) Option {
	return func(rpc *GrpcServiceClient) {
		rpc.Payment.Balance = payment.NewBalanceServiceClient(conn)
		rpc.Payment.Transaction = payment.NewTransactionServiceClient(conn)
	}
}

// WithSeatsClientService is a function
func WithSeatsClientService(conn *grpc.ClientConn) Option {
	return func(rpc *GrpcServiceClient) {
		rpc.Seats.Seats = seats.NewSeatsServiceClient(conn)
	}
}

// WithBookingClientService is a function
func WithBookingClientService(conn *grpc.ClientConn) Option {
	return func(rpc *GrpcServiceClient) {
		rpc.Booking.Order = booking.NewOrderServiceClient(conn)
	}
}
