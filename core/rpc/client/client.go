package client

// GrpcServiceClient represents collection of services
type GrpcServiceClient struct {
	Account
	Catalog
	Payment
	Seats
	Booking
}

// NewGrpcServiceClient is a constructor
func NewGrpcServiceClient(opts ...Option) *GrpcServiceClient {
	rpc := &GrpcServiceClient{}

	for _, opt := range opts {
		opt(rpc)
	}

	return rpc
}
