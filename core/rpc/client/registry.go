package client

import (
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/account"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/booking"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/catalog"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/payment"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/seats"
)

// Account is a struct
type Account struct {
	User   account.UserServiceClient
	Agent  account.AgentServiceClient
	Access account.AccessServiceClient
}

// Catalog is a struct
type Catalog struct {
	Class            catalog.ClassServiceClient
	Transport        catalog.TransportServiceClient
	Provider         catalog.ProviderServiceClient
	Passenger        catalog.PassengerServiceClient
	TransportShelter catalog.TransportShelterServiceClient
	Ticket           catalog.TicketServiceClient
}

// Payment is a struct
type Payment struct {
	Balance     payment.BalanceServiceClient
	Transaction payment.TransactionServiceClient
}

// Seats is a struct
type Seats struct {
	Seats seats.SeatsServiceClient
}

// Booking is a struct
type Booking struct {
	Order booking.OrderServiceClient
}
