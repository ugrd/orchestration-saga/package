package rpc

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/seats"
)

// CreateSeatsUpdate is a method
func (rp *RPC) CreateSeatsUpdate(ctx context.Context, req *seats.CreateSeatsUpdateRequest) (*seats.Seats, error) {
	r, err := rp.client.Seats.Seats.CreateSeatsUpdate(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// ReverseSeatsUpdate is a method
func (rp *RPC) ReverseSeatsUpdate(ctx context.Context, req *seats.ReverseSeatsUpdateRequest) (*seats.ReverseSeatsUpdateResponse, error) {
	r, err := rp.client.Seats.Seats.ReverseSeatsUpdate(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// GetSeatsByID is a method
func (rp *RPC) GetSeatsByID(ctx context.Context, req *seats.SeatsIdentifierRequest) (*seats.Seats, error) {
	r, err := rp.client.Seats.Seats.GetSeatsByID(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// GetSeatsByTicketID is a method
func (rp *RPC) GetSeatsByTicketID(ctx context.Context, req *seats.SeatsIdentifierByTicketIDRequest) (*seats.Seats, error) {
	r, err := rp.client.Seats.Seats.GetSeatsByTicketID(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// GetSeatsByTransportID is a method
func (rp *RPC) GetSeatsByTransportID(ctx context.Context, req *seats.SeatsIdentifierByTransportIDRequest) (*seats.ListSeats, error) {
	r, err := rp.client.Seats.Seats.GetSeatsByTransportID(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// GetSeatsAll is a method
func (rp *RPC) GetSeatsAll(ctx context.Context, req *seats.SeatsFilterQuery) (*seats.ListSeats, error) {
	r, err := rp.client.Seats.Seats.GetSeatsAll(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}
