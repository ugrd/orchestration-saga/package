package interceptors

import (
	"context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

// UnaryAuthClientInterceptor is a function to attach token.
func UnaryAuthClientInterceptor() grpc.UnaryClientInterceptor {
	return func(ctx context.Context, method string, req, reply interface{}, cc *grpc.ClientConn, invoker grpc.UnaryInvoker, opts ...grpc.CallOption) error {
		tkn := ctx.Value("tkn")
		if tkn == nil {
			return invoker(ctx, method, req, reply, cc, opts...)
		}

		return invoker(func(t string) context.Context {
			return metadata.AppendToOutgoingContext(ctx, "authorization", t)
		}(tkn.(string)), method, req, reply, cc, opts...)
	}
}
