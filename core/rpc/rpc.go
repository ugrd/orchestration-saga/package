package rpc

import (
	"gitlab.com/ugrd/orchestration-saga/package/core/rpc/client"
	"gitlab.com/ugrd/orchestration-saga/package/core/rpc/interfaces"
)

// RPC is a struct
type RPC struct {
	client *client.GrpcServiceClient
	interfaces.UnimplementedServerInterface
}

// NewRPC is a constructor
func NewRPC(rpc *client.GrpcServiceClient) *RPC {
	return &RPC{client: rpc}
}

var _ interfaces.AccountServiceServerInterface = &RPC{}
