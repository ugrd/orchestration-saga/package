package rpc

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/account"
)

// CreateUser is a method
func (rp *RPC) CreateUser(ctx context.Context, req *account.CreateUserRequest) (*account.User, error) {
	r, err := rp.client.Account.User.CreateUser(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// UpdateUser is a method
func (rp *RPC) UpdateUser(ctx context.Context, req *account.UpdateUserRequest) (*account.User, error) {
	r, err := rp.client.Account.User.UpdateUser(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// DeleteUser is a method
func (rp *RPC) DeleteUser(ctx context.Context, req *account.DeleteUserRequest) (*account.DeleteUserResponse, error) {
	r, err := rp.client.Account.User.DeleteUser(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// FindUser is a method
func (rp *RPC) FindUser(ctx context.Context, req *account.FindUserRequest) (*account.User, error) {
	r, err := rp.client.Account.User.FindUser(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// GetUser is a method
func (rp *RPC) GetUser(ctx context.Context, req *account.GetUserRequest) (*account.Users, error) {
	r, err := rp.client.Account.User.GetUser(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// GetMe is a method
func (rp *RPC) GetMe(ctx context.Context, req *account.GetMeRequest) (*account.User, error) {
	r, err := rp.client.Account.User.GetMe(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// Login is a method
func (rp *RPC) Login(ctx context.Context, req *account.LoginRequest) (*account.LoginResponse, error) {
	r, err := rp.client.Account.Access.Login(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// LoginAgent is a method
func (rp *RPC) LoginAgent(ctx context.Context, req *account.LoginRequest) (*account.LoginResponse, error) {
	r, err := rp.client.Account.Access.LoginAgent(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// GetRoleByID is a method
func (rp *RPC) GetRoleByID(ctx context.Context, req *account.IdentifierRequest) (*account.Role, error) {
	r, err := rp.client.Account.Agent.GetRoleByID(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// GetListRole is a method
func (rp *RPC) GetListRole(ctx context.Context, req *account.FilterQuery) (*account.ListRole, error) {
	r, err := rp.client.Account.Agent.GetListRole(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// CreateAgent is a method
func (rp *RPC) CreateAgent(ctx context.Context, req *account.CreateAgentRequest) (*account.Agent, error) {
	r, err := rp.client.Account.Agent.CreateAgent(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// UpdateAgent is a method
func (rp *RPC) UpdateAgent(ctx context.Context, req *account.UpdateAgentRequest) (*account.Agent, error) {
	r, err := rp.client.Account.Agent.UpdateAgent(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// DeleteAgent is a method
func (rp *RPC) DeleteAgent(ctx context.Context, req *account.IdentifierRequest) (*account.DeleteAgentResponse, error) {
	r, err := rp.client.Account.Agent.DeleteAgent(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// GetAgentByID is a method
func (rp *RPC) GetAgentByID(ctx context.Context, req *account.IdentifierRequest) (*account.Agent, error) {
	r, err := rp.client.Account.Agent.GetAgentByID(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// GetListAgent is a method
func (rp *RPC) GetListAgent(ctx context.Context, query *account.FilterQuery) (*account.ListAgent, error) {
	r, err := rp.client.Account.Agent.GetListAgent(ctx, query)
	if err != nil {
		return nil, err
	}

	return r, nil
}
