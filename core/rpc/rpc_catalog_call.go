package rpc

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/catalog"
)

// CreateClass is a method
func (rp *RPC) CreateClass(ctx context.Context, req *catalog.CreateClassRequest) (*catalog.Class, error) {
	r, err := rp.client.Catalog.Class.CreateClass(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// UpdateClass is a method
func (rp *RPC) UpdateClass(ctx context.Context, req *catalog.UpdateClassRequest) (*catalog.Class, error) {
	r, err := rp.client.Catalog.Class.UpdateClass(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// DeleteClass is a method
func (rp *RPC) DeleteClass(ctx context.Context, req *catalog.ClassIdentifierRequest) (*catalog.DeleteClassResponse, error) {
	r, err := rp.client.Catalog.Class.DeleteClass(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// GetClassByID is a method
func (rp *RPC) GetClassByID(ctx context.Context, req *catalog.ClassIdentifierRequest) (*catalog.Class, error) {
	r, err := rp.client.Catalog.Class.GetClassByID(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// GetListClass is a method
func (rp *RPC) GetListClass(ctx context.Context, req *catalog.ClassFilterQuery) (*catalog.ListClass, error) {
	r, err := rp.client.Catalog.Class.GetListClass(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// CreatePassenger is a method
func (rp *RPC) CreatePassenger(ctx context.Context, req *catalog.CreatePassengerRequest) (*catalog.Passenger, error) {
	r, err := rp.client.Catalog.Passenger.CreatePassenger(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// UpdatePassenger is a method
func (rp *RPC) UpdatePassenger(ctx context.Context, req *catalog.UpdatePassengerRequest) (*catalog.Passenger, error) {
	r, err := rp.client.Catalog.Passenger.UpdatePassenger(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// DeletePassenger is a method
func (rp *RPC) DeletePassenger(ctx context.Context, req *catalog.PassengerIdentifierRequest) (*catalog.DeletePassengerResponse, error) {
	r, err := rp.client.Catalog.Passenger.DeletePassenger(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// GetPassengerByID is a method
func (rp *RPC) GetPassengerByID(ctx context.Context, req *catalog.PassengerIdentifierRequest) (*catalog.Passenger, error) {
	r, err := rp.client.Catalog.Passenger.GetPassengerByID(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// GetListPassenger is a method
func (rp *RPC) GetListPassenger(ctx context.Context, req *catalog.PassengerFilterQuery) (*catalog.ListPassenger, error) {
	r, err := rp.client.Catalog.Passenger.GetListPassenger(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// CreateProvider is a method
func (rp *RPC) CreateProvider(ctx context.Context, req *catalog.CreateProviderRequest) (*catalog.Provider, error) {
	r, err := rp.client.Catalog.Provider.CreateProvider(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// DeleteProvider is a method
func (rp *RPC) DeleteProvider(ctx context.Context, req *catalog.ProviderIdentifierRequest) (*catalog.DeleteProviderResponse, error) {
	r, err := rp.client.Catalog.Provider.DeleteProvider(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// GetListProvider is a method
func (rp *RPC) GetListProvider(ctx context.Context, query *catalog.ProviderFilterQuery) (*catalog.ListProvider, error) {
	r, err := rp.client.Catalog.Provider.GetListProvider(ctx, query)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// GetProviderByID is a method
func (rp *RPC) GetProviderByID(ctx context.Context, req *catalog.ProviderIdentifierRequest) (*catalog.Provider, error) {
	r, err := rp.client.Catalog.Provider.GetProviderByID(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// UpdateProvider is a method
func (rp *RPC) UpdateProvider(ctx context.Context, req *catalog.UpdateProviderRequest) (*catalog.Provider, error) {
	r, err := rp.client.Catalog.Provider.UpdateProvider(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// CreateTransport is a method
func (rp *RPC) CreateTransport(ctx context.Context, req *catalog.CreateTransportRequest) (*catalog.Transport, error) {
	r, err := rp.client.Catalog.Transport.CreateTransport(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// DeleteTransport is a method
func (rp *RPC) DeleteTransport(ctx context.Context, req *catalog.TransportIdentifierRequest) (*catalog.DeleteTransportResponse, error) {
	r, err := rp.client.Catalog.Transport.DeleteTransport(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// GetListTransport is a method
func (rp *RPC) GetListTransport(ctx context.Context, query *catalog.TransportFilterQuery) (*catalog.ListTransport, error) {
	r, err := rp.client.Catalog.Transport.GetListTransport(ctx, query)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// GetTransportByID is a method
func (rp *RPC) GetTransportByID(ctx context.Context, req *catalog.TransportIdentifierRequest) (*catalog.Transport, error) {
	r, err := rp.client.Catalog.Transport.GetTransportByID(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// UpdateTransport is a method
func (rp *RPC) UpdateTransport(ctx context.Context, req *catalog.UpdateTransportRequest) (*catalog.Transport, error) {
	r, err := rp.client.Catalog.Transport.UpdateTransport(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// CreateTransportShelter is a method
func (rp *RPC) CreateTransportShelter(ctx context.Context, req *catalog.CreateTransportShelterRequest) (*catalog.TransportShelter, error) {
	r, err := rp.client.Catalog.TransportShelter.CreateTransportShelter(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// DeleteTransportShelter is a method
func (rp *RPC) DeleteTransportShelter(ctx context.Context, req *catalog.TransportShelterIdentifierRequest) (*catalog.DeleteTransportShelterResponse, error) {
	r, err := rp.client.Catalog.TransportShelter.DeleteTransportShelter(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// GetListTransportShelter is a method
func (rp *RPC) GetListTransportShelter(ctx context.Context, query *catalog.TransportShelterFilterQuery) (*catalog.ListTransportShelter, error) {
	r, err := rp.client.Catalog.TransportShelter.GetListTransportShelter(ctx, query)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// GetTransportShelterByID is a method
func (rp *RPC) GetTransportShelterByID(ctx context.Context, req *catalog.TransportShelterIdentifierRequest) (*catalog.TransportShelter, error) {
	r, err := rp.client.Catalog.TransportShelter.GetTransportShelterByID(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// UpdateTransportShelter is a method
func (rp *RPC) UpdateTransportShelter(ctx context.Context, req *catalog.UpdateTransportShelterRequest) (*catalog.TransportShelter, error) {
	r, err := rp.client.Catalog.TransportShelter.UpdateTransportShelter(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// CreateTicket is a method
func (rp *RPC) CreateTicket(ctx context.Context, req *catalog.CreateTicketRequest) (*catalog.Ticket, error) {
	r, err := rp.client.Catalog.Ticket.CreateTicket(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// DeleteTicket is a method
func (rp *RPC) DeleteTicket(ctx context.Context, req *catalog.TicketIdentifierRequest) (*catalog.DeleteTicketRequest, error) {
	r, err := rp.client.Catalog.Ticket.DeleteTicket(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// GetListTicket is a method
func (rp *RPC) GetListTicket(ctx context.Context, query *catalog.TicketFilterQuery) (*catalog.ListTicket, error) {
	r, err := rp.client.Catalog.Ticket.GetListTicket(ctx, query)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// GetTicketByID is a method
func (rp *RPC) GetTicketByID(ctx context.Context, req *catalog.TicketIdentifierRequest) (*catalog.Ticket, error) {
	r, err := rp.client.Catalog.Ticket.GetTicketByID(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// GetTicketByNo is a method
func (rp *RPC) GetTicketByNo(ctx context.Context, req *catalog.TicketIdentifierByNoRequest) (*catalog.Ticket, error) {
	r, err := rp.client.Catalog.Ticket.GetTicketByNo(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// UpdateTicket is a method
func (rp *RPC) UpdateTicket(ctx context.Context, req *catalog.UpdateTicketRequest) (*catalog.Ticket, error) {
	r, err := rp.client.Catalog.Ticket.UpdateTicket(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// DecreaseTicketQty is a method
func (rp *RPC) DecreaseTicketQty(ctx context.Context, req *catalog.DecreaseTicketQtyRequest) (*catalog.Ticket, error) {
	r, err := rp.client.Catalog.Ticket.DecreaseTicketQty(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// IncreaseTicketQty is a method
func (rp *RPC) IncreaseTicketQty(ctx context.Context, req *catalog.IncreaseTicketQtyRequest) (*catalog.Ticket, error) {
	r, err := rp.client.Catalog.Ticket.IncreaseTicketQty(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}
