package interfaces

import (
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/account"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/booking"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/catalog"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/payment"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/seats"
)

// AccountServiceServerInterface collects account service server
type AccountServiceServerInterface interface {
	account.UserServiceServer
	account.AgentServiceServer
	account.AccessServiceServer
}

// CatalogServiceServerInterface collects catalog service server
type CatalogServiceServerInterface interface {
	catalog.ClassServiceServer
	catalog.TransportServiceServer
	catalog.ProviderServiceServer
	catalog.PassengerServiceServer
	catalog.TransportShelterServiceServer
	catalog.TicketServiceServer
}

// PaymentServiceServerInterface is an interface
type PaymentServiceServerInterface interface {
	payment.BalanceServiceServer
	payment.TransactionServiceServer
}

// SeatsServiceServerInterface is an interface
type SeatsServiceServerInterface interface {
	seats.SeatsServiceServer
}

// BookingServiceServerInterface is an interface
type BookingServiceServerInterface interface {
	booking.OrderServiceServer
}
