package interfaces

import (
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/account"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/booking"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/catalog"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/payment"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/seats"
)

// UnimplementedServerInterface is a method
type UnimplementedServerInterface struct {
	// account
	account.UnimplementedUserServiceServer
	account.UnimplementedAgentServiceServer
	account.UnimplementedAccessServiceServer
	// catalog
	catalog.UnimplementedClassServiceServer
	catalog.UnimplementedTransportServiceServer
	catalog.UnimplementedProviderServiceServer
	catalog.UnimplementedPassengerServiceServer
	catalog.UnimplementedTransportShelterServiceServer
	catalog.UnimplementedTicketServiceServer
	// payment
	payment.UnimplementedBalanceServiceServer
	payment.UnimplementedTransactionServiceServer
	// seats
	seats.UnimplementedSeatsServiceServer
	// booking
	booking.UnimplementedOrderServiceServer
}
