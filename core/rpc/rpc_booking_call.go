package rpc

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/booking"
)

// CreateOrder is a method
func (rp *RPC) CreateOrder(ctx context.Context, req *booking.CreateOrderRequest) (*booking.Order, error) {
	r, err := rp.client.Booking.Order.CreateOrder(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// CreateOrder is a method
func (rp *RPC) GetOrderList(ctx context.Context, req *booking.OrderFilterQuery) (*booking.OrderList, error) {
	r, err := rp.client.Booking.Order.GetOrderList(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// GetOrderByID is a method
func (rp *RPC) GetOrderByID(ctx context.Context, req *booking.OrderIdentifierRequest) (*booking.Order, error) {
	r, err := rp.client.Booking.Order.GetOrderByID(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// GetOrderByToken is a method
func (rp *RPC) GetOrderByToken(ctx context.Context, req *booking.OrderIdentifierByTokenRequest) (*booking.Order, error) {
	r, err := rp.client.Booking.Order.GetOrderByToken(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}
