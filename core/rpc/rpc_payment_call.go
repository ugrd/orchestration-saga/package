package rpc

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/payment"
)

// CreateBalance is a method
func (rp *RPC) CreateBalance(ctx context.Context, req *payment.BalancePaymentRequest) (*payment.Balance, error) {
	r, err := rp.client.Payment.Balance.CreateBalance(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// CreditBalance is a method
func (rp *RPC) CreditBalance(ctx context.Context, req *payment.BalanceMutationRequest) (*payment.Balance, error) {
	r, err := rp.client.Payment.Balance.CreditBalance(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// DebitBalance is a method
func (rp *RPC) DebitBalance(ctx context.Context, req *payment.BalanceMutationRequest) (*payment.Balance, error) {
	r, err := rp.client.Payment.Balance.DebitBalance(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// GetBalanceByID is a method
func (rp *RPC) GetBalanceByID(ctx context.Context, req *payment.BalanceIdentifierRequest) (*payment.Balance, error) {
	r, err := rp.client.Payment.Balance.GetBalanceByID(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// GetBalanceByUserID is a method
func (rp *RPC) GetBalanceByUserID(ctx context.Context, req *payment.BalanceIdentifierByUserIDRequest) (*payment.Balance, error) {
	r, err := rp.client.Payment.Balance.GetBalanceByUserID(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// GetListBalance is a method
func (rp *RPC) GetListBalance(ctx context.Context, req *payment.BalanceFilterQuery) (*payment.ListBalance, error) {
	r, err := rp.client.Payment.Balance.GetListBalance(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// CreateTransaction is a method
func (rp *RPC) CreateTransaction(ctx context.Context, req *payment.CreateTransactionRequest) (*payment.Transaction, error) {
	r, err := rp.client.Payment.Transaction.CreateTransaction(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// CancelTransaction is a method
func (rp *RPC) CancelTransaction(ctx context.Context, req *payment.CancelTransactionRequest) (*payment.Transaction, error) {
	r, err := rp.client.Payment.Transaction.CancelTransaction(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// GetTransactionByID is a method
func (rp *RPC) GetTransactionByID(ctx context.Context, req *payment.TransactionIdentifierRequest) (*payment.Transaction, error) {
	r, err := rp.client.Payment.Transaction.GetTransactionByID(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// GetListTransaction is a method
func (rp *RPC) GetListTransaction(ctx context.Context, req *payment.TransactionFilterQuery) (*payment.ListTransaction, error) {
	r, err := rp.client.Payment.Transaction.GetListTransaction(ctx, req)
	if err != nil {
		return nil, err
	}

	return r, nil
}
