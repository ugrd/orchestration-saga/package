package utils

import (
	"crypto/rand"
	"math/big"
	"unicode"
)

const (
	// tokenLength is default length
	tokenLength = 15
	// charSet is a character set
	charSet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
)

// GenerateUniqueToken is a method to generate token
func GenerateUniqueToken() string {
	// convert to slice of bytes
	token := make([]byte, tokenLength)
	// get length
	charSetLength := big.NewInt(int64(len(charSet)))

	for i := 0; i < tokenLength; i++ {
		randomIndex, _ := rand.Int(rand.Reader, charSetLength)
		token[i] = charSet[randomIndex.Int64()]
	}

	runes := []rune(string(token))
	runes[0] = unicode.ToUpper(runes[0])

	return string(runes)
}
