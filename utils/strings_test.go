package utils_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/ugrd/orchestration-saga/package/utils"
	"testing"
)

func TestStringToSlug(t *testing.T) {
	txt := "Hell Yeah"
	r := utils.StringToSlug(txt)

	assert.Equal(t, "hell-yeah", r)
}
