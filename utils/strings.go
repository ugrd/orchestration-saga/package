package utils

import "strings"

// StringToSlug is a function to convert string to slug
func StringToSlug(str string) string {
	return strings.Join(strings.Split(strings.ToLower(str), " "), "-")
}
