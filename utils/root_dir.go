package utils

import (
	"path/filepath"
	"runtime"
)

// RootDir is a function
func RootDir() string {
	_, b, _, ok := runtime.Caller(0)
	if ok {
		return filepath.Join(filepath.Dir(b), "..")
	}

	return ""
}
