package utils

import (
	"encoding/json"
	"google.golang.org/genproto/googleapis/rpc/errdetails"
	"google.golang.org/grpc/status"
)

// ErrToMap is a function to convert
func ErrToMap(err error) map[string]interface{} {
	val, _ := json.Marshal(err)
	res := make(map[string]interface{})

	_ = json.Unmarshal(val, &res)

	return res
}

// ErrFields is a type
type ErrFields map[string]string

// ErrMessageFromGrpc is a struct
type ErrMessageFromGrpc struct {
	Message     string `json:"message"`
	HTTPCode    int    `json:"http_code"`
	ErrorFields ErrFields
}

// FromGRPCError is a function
func FromGRPCError(err error) *ErrMessageFromGrpc {
	ErrOtp := &ErrMessageFromGrpc{}

	ms := make(ErrFields)
	er := status.Convert(err)

	ErrOtp.Message = er.Message()
	ErrOtp.HTTPCode = HTTPStatusFromCode(er.Code())

	for _, dt := range er.Details() {
		switch t := dt.(type) {
		case *errdetails.BadRequest:
			for _, violation := range t.GetFieldViolations() {
				_, ok := ms[violation.GetField()]
				if !ok {
					ms[violation.GetField()] = violation.GetDescription()
				}
			}
		}
	}

	ErrOtp.ErrorFields = ms

	return ErrOtp
}
