package utils_test

import (
	"encoding/json"
	"errors"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ugrd/orchestration-saga/package/utils"
	"google.golang.org/genproto/googleapis/rpc/errdetails"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"net/http"
	"testing"
)

func TestErrToMap(t *testing.T) {
	msg := map[string]interface{}{
		"name":  "Invalid name",
		"email": "Invalid email",
	}

	r, _ := json.Marshal(msg)
	v := utils.ErrToMap(errors.New(string(r)))

	assert.NotNil(t, v)
}

func TestFromGRPCWithDetails(t *testing.T) {
	r := utils.FromGRPCError(errWithDetails())

	assert.NotNil(t, r)
	assert.NotEmpty(t, r.Message)
	assert.NotEmpty(t, r.HTTPCode)
	assert.NotEmpty(t, r.ErrorFields)
	assert.Equal(t, http.StatusBadRequest, r.HTTPCode)
}

func TestFromGRPCError(t *testing.T) {
	r := utils.FromGRPCError(status.Error(codes.AlreadyExists, "Already exist"))

	assert.NotNil(t, r)
	assert.NotEmpty(t, r.Message)
	assert.NotEmpty(t, r.HTTPCode)
	assert.Empty(t, r.ErrorFields)
	assert.Equal(t, http.StatusConflict, r.HTTPCode)
}

func errWithDetails() error {
	st := status.New(codes.InvalidArgument, "Fields are not valid")

	br := &errdetails.BadRequest{}
	br.FieldViolations = append(br.FieldViolations, &errdetails.BadRequest_FieldViolation{
		Field:       "email",
		Description: "Invalid email format",
	})

	br.FieldViolations = append(br.FieldViolations, &errdetails.BadRequest_FieldViolation{
		Field:       "password",
		Description: "Must be at least 10 characters",
	})

	st, _ = st.WithDetails(br)

	return st.Err()
}
