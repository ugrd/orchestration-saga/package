package utils_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/ugrd/orchestration-saga/package/utils"
	"google.golang.org/grpc/codes"
	"net/http"
	"testing"
)

func TestHTTPStatusFromCode(t *testing.T) {
	assert.Equal(t, http.StatusConflict, utils.HTTPStatusFromCode(codes.AlreadyExists))
	assert.Equal(t, http.StatusOK, utils.HTTPStatusFromCode(codes.OK))
	assert.Equal(t, http.StatusBadRequest, utils.HTTPStatusFromCode(codes.InvalidArgument))
	assert.Equal(t, http.StatusNotFound, utils.HTTPStatusFromCode(codes.NotFound))
	assert.Equal(t, http.StatusForbidden, utils.HTTPStatusFromCode(codes.PermissionDenied))
	assert.Equal(t, http.StatusUnauthorized, utils.HTTPStatusFromCode(codes.Unauthenticated))
	assert.Equal(t, http.StatusInternalServerError, utils.HTTPStatusFromCode(codes.Internal))
	assert.Equal(t, http.StatusServiceUnavailable, utils.HTTPStatusFromCode(codes.Unavailable))
}
